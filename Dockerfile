# Base Image belirtmek için kullanılan bir Dockerfile komutudur. DockerHub’da resmi olarak yayınlanan node.js Image’ını kullanarak başlıyoruz.
# Dockerfile oluştururken versiyon kullanmanız yararlı olacaktır. Çünkü “latest” versiyonun ne zaman değişeceğini bilemezsiniz.
FROM node:latest

# Mevcut dosya sisteminizdeki hangi dosyaları Docker Image’ında nereye eklemek istediğinizi belirtebilirsiniz. Format olarak <src> … <dest> şeklinde kullanılır. 
ADD yarn.lock /yarn.lock
ADD package.json /package.json


ENV NODE_PATH=/node_modules
ENV PATH=$PATH:/node_modules/.bin
RUN yarn

# Container üzerinde çalıştırılacak olan komutların nerede çalışacağını belirtir. RUN, CMD, ENTRYPOINT, COPY ve ADD komutları çalışacağı zaman bu path üzerinde çalışacaktır.
WORKDIR /app
ADD . /app

# Expose komutu ile bağlantı noktasını İşletim Sistemine gösterir. Docker Container’ındaki hangi bağlantı noktasının İşletim Sistemine bağlanacağını görmeyi kolaylaştırır.
EXPOSE 3000
EXPOSE 35729


# Container çalıştığında ilk önce yürütülecek komutu ve parametreleri ayarlar.
# “docker run <image>” çalıştırılırken verilen her komut bu komut ile birlikte işletilir ve CMD kullanılarak belirtilen tüm öğeleri geçersiz kılar. Örneğin, “docker run <image> bash” komutunda “bash” argümanı “entrypoint” sonuna eklenir.
ENTRYPOINT ["/bin/bash", "/app/run.sh"]

# Docker, Image için ana komut ayarlamak için ENTRYPOINT kullanılmasını öneriyor. Varsayılan için ise CMD kullanılmasını öneriyor. Her ikisininde birlikte kullanılmasına örnek aşağıdaki gibidir.
CMD ["start"]